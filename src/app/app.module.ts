import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { GridComponent } from './components/grid/grid.component';
import { WordsComponent } from './components/words/words.component';
import { PuzzleComponent } from './components/puzzle/puzzle.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClockComponent } from './components/clock/clock.component';
import {MatGridListModule} from '@angular/material/grid-list';

@NgModule({
  declarations: [
    AppComponent,
    GridComponent,
    WordsComponent,
    PuzzleComponent,
    ClockComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatGridListModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
