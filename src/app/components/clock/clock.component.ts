import { Component, OnInit } from '@angular/core';
import { Observable, Subscription, interval } from "rxjs";
import { SquareService } from 'src/app/services/square.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.css']
})
export class ClockComponent implements OnInit {

  private clock: Observable<number>;
  time: number = 0;
  ssoEntered: boolean = false;

  private _clockSubscription: Subscription = new Subscription;

  constructor(private squareService: SquareService) {
    this.clock = interval(1000);
  }

  ngOnInit() {
    this._clockSubscription = this.clock.subscribe(time => this.time = time);
    this.ssoEntered = true;
  }

  ngOnDestroy(): void {
    this._clockSubscription.unsubscribe();
  }

  onSubmit(ssoValue: any) {
    let correctResponseCount = 0;
    if(!ssoValue) {
      this.ssoEntered = false;
      console.log('SSO not entered');
    } else {
      correctResponseCount = this.squareService.calculateScore();
      console.log('Length : ' + correctResponseCount);
      console.log(ssoValue);
    }
  }

}
